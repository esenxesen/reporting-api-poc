# Reporting API 

The Reporting API gives you access to most of the report data in PSP. With the Reporting API you​ ​can:

● Build​ ​custom​ ​dashboards​ ​to​ ​display​ ​PSP​ ​API​ ​data.
● Save​ ​time​ ​by​ ​automating​ ​complex​ ​reporting​ ​tasks.

1.​ ​Requirement
To begin using the API, you should have received your API Key. The API Key “apiKey” has to
be​ ​added​ ​in​ ​the​ ​parameters​ ​of​ ​every​ ​request. 

2.​ ​Security
The API uses a secured and encrypted communication over the HTTPS protocol. This protocol uses a layer called SSL encryption that ensures the integrity and the confidentiality of all data transmitted over the TCP/IP network layer. The security is provided by an authentication certificate​ ​issued​ ​from​ ​an​ ​approved​ ​third​ ​party​ ​authority.
Following​ ​the​ ​discovery​ ​of​ ​a​ ​security​ ​flaw​ ​in​ ​SSLv2-3,​ ​TLS​ ​must​ ​be​ ​used​ ​to​ ​call​ ​the​ ​API.
The​ ​security​ ​is​ ​increased​ ​by​ ​a​ ​second​ ​process​ ​in​ ​order​ ​to​ ​increase​ ​the​ ​safety​ ​of​ ​the​ ​services.​ ​All​ ​the request​ ​are​ ​logged​ ​on​ ​the​ ​our​ ​side.​ ​This​ ​allows​ ​us​ ​to​ ​track​ ​and​ ​send​ ​you​ ​alerts​ ​when​ ​suspicious​ ​behaviors are​ ​detected.