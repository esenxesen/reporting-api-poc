package com.makeitconsulting.reportingapipoc.util;

public enum Operation {
    DIRECT("DIRECT"),
    REFUND("REFUND"),
    THREED("3D"),
    THREEDAUTH("3DAUTH");

    private String value;

    private Operation(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static Operation fromValue(String v) {
        for (Operation c: Operation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}