package com.makeitconsulting.reportingapipoc.util;

public enum FilterField {

    UUID("Transaction UUID"),
    EMAIL("Customer Email"),
    REFNO("Reference No"),
    CUSTOMDATA("Custom Data"),
    CARDPAN("Card PAN");

    private String value;

    private FilterField(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }


    public static FilterField fromValue(String v) {
        for (FilterField c: FilterField.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}

