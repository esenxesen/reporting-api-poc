package com.makeitconsulting.reportingapipoc.util.mapper;

import com.makeitconsulting.reportingapipoc.dto.response.CustomerResponse;
import  com.makeitconsulting.reportingapipoc.model.Customer;

public class CustomerMapper {


    public static CustomerResponse convertToDto(Customer customer) {
        CustomerResponse customerResponse = new CustomerResponse();
        customerResponse.setId(customer.getId());
        customerResponse.setCreatedAt(customer.getCreatedAt());
        customerResponse.setUpdatedAt(customer.getUpdatedAt());
        customerResponse.setDeletedAt(customer.getDeletedAt());
        customerResponse.setNumber(customer.getNumber());
        customerResponse.setExpiryMonth(customer.getExpiryMonth());
        customerResponse.setExpiryYear(customer.getExpiryYear());
        customerResponse.setStartMonth(customer.getStartMonth());
        customerResponse.setStartYear(customer.getStartYear());
        customerResponse.setIssueNumber(customer.getIssueNumber());
        customerResponse.setEmail(customer.getEmail());
        customerResponse.setBirthday(customer.getBirthday());
        customerResponse.setGender(customer.getGender());
        customerResponse.setBillingTitle(customer.getBillingAdressInfo().getTitle());
        customerResponse.setBillingFirstName(customer.getBillingAdressInfo().getFirstName());
        customerResponse.setBillingLastName(customer.getBillingAdressInfo().getLastName());
        customerResponse.setBillingCompany(customer.getBillingAdressInfo().getCompany());
        customerResponse.setBillingAddress1(customer.getBillingAdressInfo().getAddress1());
        customerResponse.setBillingAddress2(customer.getBillingAdressInfo().getAddress2());
        customerResponse.setBillingCity(customer.getBillingAdressInfo().getCity());
        customerResponse.setBillingPostcode(customer.getBillingAdressInfo().getPostcode());
        customerResponse.setBillingState(customer.getBillingAdressInfo().getState());
        customerResponse.setBillingCountry(customer.getBillingAdressInfo().getCountry());
        customerResponse.setBillingPhone​(customer.getBillingAdressInfo().getPhone​());
        customerResponse.setBillingFax(customer.getBillingAdressInfo().getFax());
        customerResponse.setShippingTitle(customer.getShippingAddressInfo().getTitle());
        customerResponse.setShippingFirstName(customer.getShippingAddressInfo().getFirstName());
        customerResponse.setShippingLastName​(customer.getShippingAddressInfo().getLastName());
        customerResponse.setShippingCompany(customer.getShippingAddressInfo().getCompany());
        customerResponse.setShippingAddress1​(customer.getShippingAddressInfo().getAddress1());
        customerResponse.setShippingAddress2​(customer.getShippingAddressInfo().getAddress2());
        customerResponse.setShippingCity​(customer.getShippingAddressInfo().getCity());
        customerResponse.setShippingPostcode​(customer.getShippingAddressInfo().getPostcode());
        customerResponse.setShippingState(customer.getShippingAddressInfo().getState());
        customerResponse.setShippingCountry(customer.getShippingAddressInfo().getCountry());
        customerResponse.setShippingPhone(customer.getShippingAddressInfo().getPhone​());
        customerResponse.setShippingFax(customer.getShippingAddressInfo().getFax());


        return customerResponse;
    }
}
