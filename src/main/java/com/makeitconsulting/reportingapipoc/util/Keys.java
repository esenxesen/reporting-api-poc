package com.makeitconsulting.reportingapipoc.util;

public class Keys {
    public enum StatusKeys {
        APPROVED,
        WAITING,
        DECLINED,
        ERROR;
    }

    public enum PaymentMethod {

        CREDITCARD,
        CUP,
        IDEAL,
        GIROPAY,
        MISTERCASH,
        STORED,
        PAYTOCARD,
        CEPBANK,
        CITADEL
    }

}