package com.makeitconsulting.reportingapipoc.repository;

import com.makeitconsulting.reportingapipoc.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
@Transactional
public interface TransactionRepository extends CrudRepository<Transaction,String>,JpaSpecificationExecutor<Transaction> {


    Optional<Transaction> getTransactionByTransactionId(String transactionId);

    Page<Transaction> findAll(Pageable pageable);


}
