package com.makeitconsulting.reportingapipoc.repository;

import com.makeitconsulting.reportingapipoc.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

  boolean existsByEmail(String email);

  User findByEmail(String email);

}
