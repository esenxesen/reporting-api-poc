package com.makeitconsulting.reportingapipoc.repository;

import com.makeitconsulting.reportingapipoc.model.Acquirer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcquirerRepository  extends JpaRepository<Acquirer, Long> {
}
