package com.makeitconsulting.reportingapipoc.repository;

import com.makeitconsulting.reportingapipoc.model.MerchantTransactions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MerchantTransactionsRepository extends JpaRepository<MerchantTransactions, Long> {

    List<MerchantTransactions> findByMerchantId(Long merchantId);
}
