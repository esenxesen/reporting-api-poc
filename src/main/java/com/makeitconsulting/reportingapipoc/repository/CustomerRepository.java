package com.makeitconsulting.reportingapipoc.repository;

import com.makeitconsulting.reportingapipoc.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findById(Long customerId);
}
