package com.makeitconsulting.reportingapipoc.repository;

import com.makeitconsulting.reportingapipoc.model.Fx;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FxRepository  extends JpaRepository<Fx, Long> {
}
