package com.makeitconsulting.reportingapipoc;

import com.makeitconsulting.reportingapipoc.model.*;
import com.makeitconsulting.reportingapipoc.repository.*;
import com.makeitconsulting.reportingapipoc.service.UserService;
import com.makeitconsulting.reportingapipoc.util.ErrorCode;
import com.makeitconsulting.reportingapipoc.util.Keys;
import com.makeitconsulting.reportingapipoc.util.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;

@SpringBootApplication
public class ReportingApiPocApplication implements CommandLineRunner {
	@Autowired
	UserService userService;

	@Autowired
	AddressRepository addressRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	MerchantRepository merchantRepository;

	@Autowired
	AcquirerRepository acquirerRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	FxRepository fxRepository;

	@Autowired
	MerchantTransactionsRepository merchantTransactionsRepository;

	@Autowired
	AgentRepository agentRepository;

	public static void main(String[] args) {
		SpringApplication.run(ReportingApiPocApplication.class, args);
	}

	@Override
	public void run(String... params) throws Exception {


		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date parsedDate = formatter.parse("1986-03-20 12:09:10");

		User admin = new User();
		admin.setPassword("12345");
		admin.setEmail("merchant@test.com");
		admin.setRoles(new ArrayList<Role>(Arrays.asList(Role.ADMIN)));

		userService.save(admin);

		User client = new User();
		client.setPassword("client");
		client.setEmail("client@email.com");
		client.setRoles(new ArrayList<Role>(Arrays.asList(Role.CLIENT)));

		userService.save(client);
		Address address = new Address();
		address.setFirstName("Micheal");
		address.setLastName("Kara");
		address.setAddress1("test address");
		address.setCity("Antalya");
		address.setPostcode("07070");
		address.setCountry("TR");
		address.setBilling(true);
		address.setShipping(true);

		addressRepository.save(address);

		Customer customer = new Customer();
		customer.setNumber("401288XXXXXX1881");
		customer.setExpiryMonth("6");
		customer.setExpiryYear("2017");
		customer.setEmail("micheal@gmail.com");
		customer.setCreatedAt(new Date());
		customer.setUpdatedAt(LocalDate.now());



		customer.setBirthday(parsedDate);

		customer.setBillingAdressInfo(address);
		customer.setShippingAddressInfo(address);
		customerRepository.save(customer);

		Merchant merchant = new Merchant();
		merchant.setId(Long.valueOf(1));
		merchant.setName("esem");
		merchantRepository.save(merchant);

		Merchant merchant1 = new Merchant();
		merchant1.setId(Long.valueOf(2));
		merchant1.setName("sali");
		merchantRepository.save(merchant1);


		Merchant merchant2 = new Merchant();
		merchant2.setId(Long.valueOf(3));
		merchant2.setName("alex");
		merchantRepository.save(merchant2);

		Acquirer acquirer = new Acquirer();
		acquirer.setId(Long.valueOf(1));
		acquirer.setCode("HB");
		acquirer.setName("HSBC BANK");
		acquirer.setType(Keys.PaymentMethod.CEPBANK.toString());
		acquirerRepository.save(acquirer);

		Acquirer acquirer1 = new Acquirer();
		acquirer1.setId(Long.valueOf(2));
		acquirer1.setCode("AB");
		acquirer1.setName("A BANK");
		acquirer1.setType(Keys.PaymentMethod.CREDITCARD.toString());
		acquirerRepository.save(acquirer1);

		Acquirer acquirer2 = new Acquirer();
		acquirer2.setId(Long.valueOf(3));
		acquirer2.setCode("CB");
		acquirer2.setName("C BANK");
		acquirer2.setType(Keys.PaymentMethod.CITADEL.toString());
		acquirerRepository.save(acquirer2);
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");

		Date parsedDate1 = formatter2.parse("2019-03-20");
		Date parsedDate2 = formatter2.parse("2019-03-21");


		Fx fx = new Fx();
		fx.setAmount(100);
		fx.setCurrency(Currency.getInstance("USD"));
		fxRepository.save(fx);

		Fx fx1 = new Fx();
		fx1.setAmount(200);
		fx1.setCurrency(Currency.getInstance("USD"));
		fxRepository.save(fx1);

		Fx fx2 = new Fx();
		fx2.setAmount(300);
		fx2.setCurrency(Currency.getInstance("GBP"));
		fxRepository.save(fx2);

		Fx fx3 = new Fx();
		fx3.setAmount(400);
		fx3.setCurrency(Currency.getInstance("GBP"));
		fxRepository.save(fx3);

		Fx fx4 = new Fx();
		fx4.setAmount(500);
		fx4.setCurrency(Currency.getInstance("EUR"));
		fxRepository.save(fx4);

		Agent agent = new Agent();
		agent.setCustomerIp("192.168.1.2");
		agent.setCustomerUserAgent("Agent");
		agent.setId(Long.valueOf(1));
		agent.setMerchantIp("192.168.1.2");
		agentRepository.save(agent);


		MerchantTransactions merchantTransactions = new MerchantTransactions();
		merchantTransactions.setReferenceNo("reference_5617ae66281ee");
		merchantTransactions.setMerchantId(Long.valueOf(1));
		merchantTransactions.setStatus(Keys.StatusKeys.APPROVED);
		merchantTransactions.setChainId("5617ae666b4cb");
		merchantTransactions.setChannel("API");
		merchantTransactions.setAgentInfoId(Long.valueOf(1));
		merchantTransactions.setOperation(Operation.DIRECT);
		merchantTransactions.setFxTransactionId(Long.valueOf(1));
		merchantTransactions.setUpdatedAt(parsedDate);
		merchantTransactions.setCreatedAt(parsedDate);
		merchantTransactions.setId(Long.valueOf(1));
		merchantTransactions.setAcquirerTransactionId(Long.valueOf(1));
		merchantTransactions.setCode("00");
		merchantTransactions.setMessage("Waiting");
		merchantTransactions.setTransactionId("1-1444392550-0");
		merchantTransactions.setAgent(agent);
		merchantTransactionsRepository.save(merchantTransactions);


		MerchantTransactions merchantTransactions1 = new MerchantTransactions();
		merchantTransactions1.setReferenceNo("reference_5617ae66281ee");
		merchantTransactions1.setMerchantId(Long.valueOf(1));
		merchantTransactions1.setStatus(Keys.StatusKeys.DECLINED);
		merchantTransactions1.setChainId("5617ae666b4cb");
		merchantTransactions1.setChannel("API");
		merchantTransactions1.setAgentInfoId(Long.valueOf(1));
		merchantTransactions1.setOperation(Operation.DIRECT);
		merchantTransactions1.setFxTransactionId(Long.valueOf(1));
		merchantTransactions1.setUpdatedAt(parsedDate);
		merchantTransactions1.setCreatedAt(parsedDate);
		merchantTransactions1.setId(Long.valueOf(2));
		merchantTransactions1.setAcquirerTransactionId(Long.valueOf(1));
		merchantTransactions1.setCode("00");
		merchantTransactions1.setMessage("Waiting");
		merchantTransactions1.setTransactionId("1-1444392550-1");
		merchantTransactions1.setAgent(agent);
		merchantTransactionsRepository.save(merchantTransactions1);

		Transaction transaction = new Transaction();
		transaction.setFromDate(parsedDate1);
		transaction.setToDate(parsedDate2);
		transaction.setTransactionId("1-1444392550-0");
		transaction.setFx(fx);
		transaction.setMerchant(merchant);
		transaction.setAcquirer(acquirer);
		transaction.setCustomerInfo(customer);
		transactionRepository.save(transaction);


		Transaction transaction2 = new Transaction();
		transaction2.setFromDate(parsedDate1);
		transaction2.setToDate(parsedDate2);
		transaction2.setTransactionId("1-1444392550-1");
		transaction2.setFx(fx1);
		transaction2.setMerchant(merchant);
		transaction2.setAcquirer(acquirer);
		transaction2.setCustomerInfo(customer);
		transactionRepository.save(transaction2);

		Transaction transaction3 = new Transaction();
		transaction3.setFromDate(parsedDate1);
		transaction3.setToDate(parsedDate2);
		transaction3.setTransactionId("1-1444392550-2");
		transaction3.setFx(fx2);
		transaction3.setMerchant(merchant);
		transaction3.setAcquirer(acquirer);
		transaction3.setCustomerInfo(customer);
		transactionRepository.save(transaction3);

		Transaction transaction4 = new Transaction();
		transaction4.setFromDate(parsedDate1);
		transaction4.setToDate(parsedDate2);
		transaction4.setTransactionId("1-1444392550-3");
		transaction4.setFx(fx3);
		transaction4.setMerchant(merchant2);
		transaction4.setAcquirer(acquirer2);
		transaction4.setCustomerInfo(customer);
		transactionRepository.save(transaction4);

		Transaction transaction5 = new Transaction();
		transaction5.setFromDate(parsedDate1);
		transaction5.setToDate(parsedDate2);
		transaction5.setTransactionId("1-1444392550-4");
		transaction5.setFx(fx4);
		transaction5.setMerchant(merchant);
		transaction5.setAcquirer(acquirer);
		transaction5.setCustomerInfo(customer);
		transactionRepository.save(transaction5);


		Transaction transaction6 = new Transaction();
		transaction6.setFromDate(parsedDate1);
		transaction6.setToDate(parsedDate2);
		transaction6.setTransactionId("1-1444392550-5");
		transaction6.setFx(fx2);
		transaction6.setMerchant(merchant);
		transaction6.setAcquirer(acquirer2);
		transaction6.setCustomerInfo(customer);
		transaction6.setErrorCode(ErrorCode.INVALID_TRANSACTION);
		transactionRepository.save(transaction6);


		Transaction transaction7 = new Transaction();
		transaction7.setFromDate(parsedDate1);
		transaction7.setToDate(parsedDate2);
		transaction7.setTransactionId("1-1444392550-6");
		transaction7.setFx(fx4);
		transaction7.setMerchant(merchant2);
		transaction7.setAcquirer(acquirer2);
		transaction7.setCustomerInfo(customer);
		transactionRepository.save(transaction7);
	}
}
