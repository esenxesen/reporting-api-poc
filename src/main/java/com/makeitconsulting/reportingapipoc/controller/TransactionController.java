package com.makeitconsulting.reportingapipoc.controller;


import com.makeitconsulting.reportingapipoc.dto.request.TransactionIdRequest;
import com.makeitconsulting.reportingapipoc.dto.request.TransactionListRequest;
import com.makeitconsulting.reportingapipoc.dto.request.TransactionsReportRequest;
import com.makeitconsulting.reportingapipoc.dto.response.TransactionDetailResponse;
import com.makeitconsulting.reportingapipoc.dto.response.TransactionListResponse;
import com.makeitconsulting.reportingapipoc.dto.response.TransactionReportResponse;
import com.makeitconsulting.reportingapipoc.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v3")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping
    @RequestMapping(value = "/transactions/report")
    public  ResponseEntity<TransactionReportResponse> getTransactionReport(@Valid @RequestBody TransactionsReportRequest transactionsReportRequest)
    {
        return new ResponseEntity<>(transactionService.getTransactionReport(transactionsReportRequest), HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping(value = "/transaction")
    public ResponseEntity<TransactionDetailResponse> getTransactionDetail(@Valid @RequestBody TransactionIdRequest transactionId)
    {
        return new ResponseEntity<>(transactionService.getTransactionDetail(transactionId), HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping(value = "/transaction/list")
    public ResponseEntity<TransactionListResponse> getTransactionList(@RequestParam(defaultValue= "1" ,required = false) int page, @RequestBody(required = false) TransactionListRequest transactionListRequest, HttpServletRequest request)
    {
        return new ResponseEntity<>(transactionService.getTransactionList(page ,transactionListRequest, request.getRequestURL().toString()), HttpStatus.OK);
    }
}
