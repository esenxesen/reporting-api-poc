package com.makeitconsulting.reportingapipoc.controller;

import io.swagger.annotations.*;
import com.makeitconsulting.reportingapipoc.dto.request.UserLoginRequest;
import com.makeitconsulting.reportingapipoc.dto.response.JwtTokenResponse;
import com.makeitconsulting.reportingapipoc.security.JwtTokenFilterConfigurer;
import com.makeitconsulting.reportingapipoc.service.UserService;
import com.makeitconsulting.reportingapipoc.util.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v3/merchant/user")
@Api(tags = "users")
public class UserController {


  private static Logger log = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService userService;

  @PostMapping("/login")
  @ApiOperation(value = "${UserController.login}")
  @ApiResponses(value = {//
      @ApiResponse(code = 400, message = "Something went wrong"), //
      @ApiResponse(code = 422, message = "Invalid username/password supplied")})
  public ResponseEntity<JwtTokenResponse> login(@ApiParam("Username") @RequestBody UserLoginRequest user) {
    log.debug("POST received - serializing LoginForm: " + user.getPassword() + " " + user.getEmail());

  String jwt = userService.login(user.getEmail(), user.getPassword());
  HttpHeaders httpHeaders = new HttpHeaders();
  httpHeaders.add(JwtTokenFilterConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
  return new ResponseEntity<>(new JwtTokenResponse("Bearer " + jwt, Keys.StatusKeys.APPROVED), httpHeaders, HttpStatus.OK);

  }
}
