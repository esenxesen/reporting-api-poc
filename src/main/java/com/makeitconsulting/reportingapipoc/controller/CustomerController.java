package com.makeitconsulting.reportingapipoc.controller;

import com.makeitconsulting.reportingapipoc.dto.request.TransactionIdRequest;
import com.makeitconsulting.reportingapipoc.dto.response.CustomerResponse;
import com.makeitconsulting.reportingapipoc.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v3/client")
public class CustomerController {

    private static Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    public ResponseEntity<CustomerResponse> getCustomerInfo(@Valid @RequestBody TransactionIdRequest transactionId ){
        log.info("Customer information for {} transaction will be gathered",transactionId.getTransactionId());
        return new ResponseEntity<>(customerService.getCustomerInfo(transactionId), HttpStatus.OK);

    }

}
