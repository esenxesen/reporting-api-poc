package com.makeitconsulting.reportingapipoc.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.makeitconsulting.reportingapipoc.dto.builder.TransactionDetailBuilder;
import com.makeitconsulting.reportingapipoc.model.*;

import java.util.List;

public class TransactionDetailResponse {

    private Fx fx;
    private CustomerResponse customerInfo;
    private Merchant merchant;
    private List<MerchantTransactions> merchantTransactions;
    private Acquirer acquirerTransactions;
    private Ipn ipn;
    private Boolean refundable;

    public Boolean getRefundable() {
        return refundable;
    }

    public void setRefundable(Boolean refundable) {
        this.refundable = refundable;
    }

    public Ipn getIpn() {
        return ipn;
    }

    public void setIpn(Ipn ipn) {
        this.ipn = ipn;
    }

    public Fx getFx() {
        return fx;
    }

    public void setFx(Fx fx) {
        this.fx = fx;
    }
    @JsonProperty("customerInfo")
    public CustomerResponse getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerResponse customerInfo) {
        this.customerInfo = customerInfo;
    }
    @JsonProperty("merchant")
    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }
    @JsonProperty("transaction")
    public List<MerchantTransactions> getMerchantTransactions() {
        return merchantTransactions;
    }

    public void setMerchantTransactions(List<MerchantTransactions> merchantTransactions) {
        this.merchantTransactions = merchantTransactions;
    }

    @JsonProperty("acquirerTransactions")
    public Acquirer getAcquirerTransactions() {
        return acquirerTransactions;
    }

    public void setAcquirerTransactions(Acquirer acquirerTransactions) {
        this.acquirerTransactions = acquirerTransactions;
    }


    public static TransactionDetailBuilder create(){
        return new TransactionDetailBuilder();
    }

}
