package com.makeitconsulting.reportingapipoc.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.makeitconsulting.reportingapipoc.util.Keys;

public class JwtTokenResponse {

    private String token;
    private Keys.StatusKeys status;

    public JwtTokenResponse(String token, Keys.StatusKeys status) {
        this.token = token;
        this.status = status;
    }

    @JsonProperty("token")
    String getToken() {
        return token;
    }

    void setToken(String token) {
        this.token = token;
    }
    @JsonProperty("status")
    public Keys.StatusKeys getStatus() {
        return status;
    }

    public void setStatus(Keys.StatusKeys status) {
        this.status = status;
    }
}
