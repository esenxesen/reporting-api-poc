package com.makeitconsulting.reportingapipoc.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.makeitconsulting.reportingapipoc.dto.FxDto;
import com.makeitconsulting.reportingapipoc.util.Keys;

import java.util.List;

public class TransactionReportResponse {

    private List<FxDto> response;
    private Keys.StatusKeys status;

    public TransactionReportResponse(List<FxDto> response, Keys.StatusKeys status) {
        this.response = response;
        this.status = status;
    }

    @JsonProperty("response")
    List<FxDto> getResponse() {
        return response;
    }

    void setResponse(List<FxDto> token) {
        this.response = response;
    }

    @JsonProperty("status")
    public Keys.StatusKeys getStatus() {
        return status;
    }

    public void setStatus(Keys.StatusKeys status) {
        this.status = status;
    }
}

