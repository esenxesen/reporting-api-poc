package com.makeitconsulting.reportingapipoc.dto.response;

import java.util.List;

public class TransactionListResponse {

    private Integer perPage;
    private Integer currentPage;
    private String nextPageUrl;
    private String prevPageUrl;
    private Integer from;
    private Integer to;
    private List<TransactionDetailResponse> data;


    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public String getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(String prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public List<TransactionDetailResponse> getData() {
        return data;
    }

    public void setData(List<TransactionDetailResponse> data) {
        this.data = data;
    }
}
