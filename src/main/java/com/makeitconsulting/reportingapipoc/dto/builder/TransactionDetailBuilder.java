package com.makeitconsulting.reportingapipoc.dto.builder;

import com.makeitconsulting.reportingapipoc.dto.response.CustomerResponse;
import com.makeitconsulting.reportingapipoc.dto.response.TransactionDetailResponse;
import com.makeitconsulting.reportingapipoc.model.*;

import java.util.List;

public class TransactionDetailBuilder {

    private TransactionDetailResponse supplier = new TransactionDetailResponse();

    public TransactionDetailResponse build() {
        return supplier;
    }


    public TransactionDetailBuilder withFx(Fx fx) {
        this.supplier.setFx(fx);
        return this;
    }

    public TransactionDetailBuilder withCustomerInfo(CustomerResponse customerInfo) {
        this.supplier.setCustomerInfo(customerInfo);
        return this;
    }
    public TransactionDetailBuilder withMerchant(Merchant merchant) {
        this.supplier.setMerchant(merchant);
        return this;
    }
    public TransactionDetailBuilder withMerchantTransactions(List<MerchantTransactions> merchantTransactions) {
        this.supplier.setMerchantTransactions(merchantTransactions);
        return this;
    }
    public TransactionDetailBuilder withAcquirer(Acquirer acquirerTransactions) {
        this.supplier.setAcquirerTransactions(acquirerTransactions);
        return this;
    }
    public TransactionDetailBuilder withIpn(Ipn ipn) {
        this.supplier.setIpn(ipn);
        return this;
    }
    public TransactionDetailBuilder withRefundable(boolean refundable) {
        this.supplier.setRefundable(refundable);
        return this;
    }

}
