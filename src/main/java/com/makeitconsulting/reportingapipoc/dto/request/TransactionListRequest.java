package com.makeitconsulting.reportingapipoc.dto.request;

import com.makeitconsulting.reportingapipoc.util.ErrorCode;
import com.makeitconsulting.reportingapipoc.util.FilterField;
import com.makeitconsulting.reportingapipoc.util.Keys;
import com.makeitconsulting.reportingapipoc.util.Operation;

import java.util.Date;
import java.util.Map;
import java.util.logging.Filter;

public class TransactionListRequest {

    private Date fromDate;
    private Date toDate;
    private String status;
    private String  operation;
    private Integer merchantId;
    private Integer acquirerId;
    private Keys.PaymentMethod paymentMethod;
    private String  errorCode;
    private String filterField;
    private String filterValue;
    private Integer page;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getAcquirerId() {
        return acquirerId;
    }

    public void setAcquirerId(Integer acquirerId) {
        this.acquirerId = acquirerId;
    }

    public Keys.PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Keys.PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getFilterField() {
        return filterField;
    }

    public void setFilterField(String filterField) {
        this.filterField = filterField;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
