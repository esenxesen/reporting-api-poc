package com.makeitconsulting.reportingapipoc.dto.request;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class TransactionsReportRequest {

    @NotNull(message = "Please provide fromDate")
    private Date fromDate;
    @NotNull(message = "Please provide toDate")
    private Date toDate;
    private Integer merchant;
    private Integer acquirer;

    public Integer getMerchant() {
        return merchant;
    }

    public void setMerchant(Integer merchant) {
        this.merchant = merchant;
    }

    public Integer getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(Integer acquirer) {
        this.acquirer = acquirer;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}