package com.makeitconsulting.reportingapipoc.dto.request;

import io.swagger.annotations.ApiModelProperty;

public class UserLoginRequest {
  
  @ApiModelProperty(position = 0)
  private String password;
  @ApiModelProperty(position = 1)
  private String email;


 public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
