package com.makeitconsulting.reportingapipoc.dto.request;

import javax.validation.constraints.NotNull;

public class TransactionIdRequest {

    @NotNull
    private String transactionId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
