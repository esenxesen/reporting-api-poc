package com.makeitconsulting.reportingapipoc.dto;

import java.math.BigDecimal;
import java.util.Currency;

public class FxDto {

    private BigDecimal count;
    private Currency currency;
    private BigDecimal total;


    public FxDto(BigDecimal count, BigDecimal total) {
        this.count = count;
        this.total =  total;
    }
    public FxDto(Currency currency ,BigDecimal count, BigDecimal total) {
        this.count = count;
        this.total = total;

        this.currency = currency;
    }
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
