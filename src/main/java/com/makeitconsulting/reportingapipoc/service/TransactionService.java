package com.makeitconsulting.reportingapipoc.service;


import com.makeitconsulting.reportingapipoc.dto.FxDto;
import com.makeitconsulting.reportingapipoc.dto.builder.TransactionDetailBuilder;
import com.makeitconsulting.reportingapipoc.dto.request.TransactionIdRequest;
import com.makeitconsulting.reportingapipoc.dto.request.TransactionListRequest;
import com.makeitconsulting.reportingapipoc.dto.request.TransactionsReportRequest;
import com.makeitconsulting.reportingapipoc.dto.response.CustomerResponse;
import com.makeitconsulting.reportingapipoc.dto.response.TransactionDetailResponse;
import com.makeitconsulting.reportingapipoc.dto.response.TransactionListResponse;
import com.makeitconsulting.reportingapipoc.dto.response.TransactionReportResponse;
import com.makeitconsulting.reportingapipoc.model.*;
import com.makeitconsulting.reportingapipoc.repository.MerchantTransactionsRepository;
import com.makeitconsulting.reportingapipoc.repository.TransactionRepository;
import com.makeitconsulting.reportingapipoc.util.ErrorCode;
import com.makeitconsulting.reportingapipoc.util.FilterField;
import com.makeitconsulting.reportingapipoc.util.Keys;
import com.makeitconsulting.reportingapipoc.util.Operation;
import com.makeitconsulting.reportingapipoc.util.mapper.CustomerMapper;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Value("${transaction.list.perPage}")
    private int perPage;


    @Autowired
    private MerchantTransactionsRepository merchantTransactionsRepository;

    public  TransactionReportResponse getTransactionReport(TransactionsReportRequest transactionsReportRequest){
        Map<Currency, FxDto> fxMap = findByCriteria(transactionsReportRequest)
                .stream()
                .map(transaction -> transaction.getFx()).collect(Collectors.toList())
                .stream().collect(
                        Collectors.groupingBy(Fx::getCurrency,
                                Collectors.collectingAndThen(Collectors.summarizingLong(Fx::getAmount),
                                        dss -> new FxDto( new BigDecimal(dss.getCount()), new BigDecimal(dss.getSum())))));



        List<FxDto> list = fxMap.entrySet().stream().sorted(Comparator.comparing(e -> e.getKey().toString()))
                .map(e -> new FxDto(e.getKey(), e.getValue().getTotal(),e.getValue().getCount())).collect(Collectors.toList());

        return new TransactionReportResponse(list,Keys.StatusKeys.APPROVED);

    }

    public TransactionDetailResponse getTransactionDetail(TransactionIdRequest request){
        Optional<Transaction> transaction=transactionRepository.getTransactionByTransactionId(request.getTransactionId());

        TransactionDetailBuilder builder = TransactionDetailResponse.create();

        if(transaction.isPresent()) {
            Fx fx = transaction.get().getFx();
            CustomerResponse customerInfo = CustomerMapper.convertToDto(transaction.get().getCustomerInfo());
            Merchant merchant = transaction.get().getMerchant();
            Acquirer acquirer = transaction.get().getAcquirer();
            List<MerchantTransactions> merchantTransactions = merchantTransactionsRepository.findByMerchantId(merchant.getId());

            if(fx != null)  builder.withFx(fx) ;
            if(acquirer != null) builder.withAcquirer(acquirer);
            if(customerInfo != null) builder.withCustomerInfo(customerInfo);
            if(merchant != null) builder.withMerchant(merchant);
            if(merchantTransactions != null) builder.withMerchantTransactions(merchantTransactions);

        }
        return builder.build();

    }

    public  TransactionListResponse getTransactionList(int pageNumber , TransactionListRequest request, String url){
        Page<Transaction> transactions =null;
        transactions = request == null ? transactionRepository.findAll(constructPageSpecification(pageNumber)) : findListByCriteria(constructPageSpecification(pageNumber), request);
        List<TransactionDetailResponse> data = new ArrayList<TransactionDetailResponse>();
        transactions.getContent().stream().forEach(transaction -> {
            TransactionDetailBuilder builder = TransactionDetailResponse.create();
            Fx fx = transaction.getFx();
            CustomerResponse customerInfo = CustomerMapper.convertToDto(transaction.getCustomerInfo());
            Merchant merchant = transaction.getMerchant();
            Acquirer acquirer = transaction.getAcquirer();
            List<MerchantTransactions> merchantTransactions = merchantTransactionsRepository.findByMerchantId(merchant.getId());
            Ipn ipn = transaction.getIpn();

            Boolean refundable = transaction.getRefundable();

            if(fx != null)  builder.withFx(fx) ;
            if(acquirer != null) builder.withAcquirer(acquirer);
            if(customerInfo != null) builder.withCustomerInfo(customerInfo);
            if(merchant != null) builder.withMerchant(merchant);
            if(merchantTransactions != null && merchantTransactions.size() > 0) builder.withMerchantTransactions(merchantTransactions);
            if(ipn != null) builder.withIpn(ipn);
            if(refundable != null )builder.withRefundable(refundable);

            data.add(builder.build());

        });


        int from = (pageNumber-1)*perPage +1;
        int to = from + transactions.getContent().size()-1;
        String nextPage = transactions.getTotalElements() > to ? url+"?page="+(pageNumber+1) : null;
        String prevPage = pageNumber >1 ? url+"?page="+(pageNumber-1) : null;

        TransactionListResponse response = new TransactionListResponse();
        response.setPerPage(perPage);
        response.setCurrentPage(pageNumber);
        response.setNextPageUrl(nextPage);
        response.setPrevPageUrl(prevPage);
        response.setData(data);
        response.setFrom(from);
        response.setTo(to);


        return  response;


    }

    public List<Transaction> findByCriteria(TransactionsReportRequest transactionsReportRequest){
        return transactionRepository.findAll(new Specification<Transaction>() {
            @Override
            public Predicate toPredicate(Root<Transaction> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("fromDate"), transactionsReportRequest.getFromDate())));
                predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("toDate"), transactionsReportRequest.getToDate())));
                if(transactionsReportRequest.getMerchant() != null) {
                    Path<Merchant> merchant = root.get("merchant");
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(merchant.get("id"), transactionsReportRequest.getMerchant())));
                }
                if(transactionsReportRequest.getAcquirer() != null){
                    Path<Acquirer> acquirer = root.get("acquirer");

                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(acquirer.get("id"), transactionsReportRequest.getAcquirer())));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });
    }
    private Pageable constructPageSpecification(int pageIndex) {
        Pageable pageSpecification = new PageRequest(pageIndex-1, perPage);
        return pageSpecification;
    }

    public Page<Transaction> findListByCriteria(Pageable pageNumber, TransactionListRequest request){



        return transactionRepository.findAll(new Specification<Transaction>() {
            @Override
            public Predicate toPredicate(Root<Transaction> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if(request.getFromDate() != null)
                    predicates.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("fromDate"), request.getFromDate())));

                if(request.getToDate() != null)
                    predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("toDate"), request.getToDate())));

                if(request.getStatus() != null) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("status"), request.getStatus())));
                }

                if(request.getOperation() != null) {

                    Operation operation = Operation.fromValue(request.getOperation());
                    predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("operation"), operation)));
                }

                if(request.getMerchantId() != null) {
                    Path<Merchant> merchant = root.get("merchant");
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(merchant.get("id"), request.getMerchantId())));
                }

                if(request.getAcquirerId() != null){
                    Path<Acquirer> acquirer = root.get("acquirer");
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(acquirer.get("id"), request.getAcquirerId())));
                }

                if(request.getPaymentMethod() != null)
                {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("paymentMethod"), request.getPaymentMethod())));
                }

                if(request.getErrorCode() != null )
                {
                    ErrorCode error = ErrorCode.fromValue(request.getErrorCode());
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("errorCode"), error)));
                }

                if(request.getFilterField() != null)
                {
                    FilterField filterField = FilterField.fromValue(request.getFilterField());
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("filterField"), filterField)));
                }

                if(request.getFilterValue() != null)
                {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("filterValue"), request.getFilterValue())));
                }


                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));            }
        }, pageNumber);

    }
}
