package com.makeitconsulting.reportingapipoc.service;

import com.makeitconsulting.reportingapipoc.dto.request.TransactionIdRequest;
import com.makeitconsulting.reportingapipoc.dto.response.CustomerResponse;
import com.makeitconsulting.reportingapipoc.exception.CustomException;
import com.makeitconsulting.reportingapipoc.model.Customer;
import com.makeitconsulting.reportingapipoc.model.Transaction;
import com.makeitconsulting.reportingapipoc.repository.CustomerRepository;
import com.makeitconsulting.reportingapipoc.repository.TransactionRepository;
import com.makeitconsulting.reportingapipoc.util.mapper.CustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public CustomerResponse getCustomerInfo(TransactionIdRequest request){
        Optional<Transaction> transaction = transactionRepository.getTransactionByTransactionId(request.getTransactionId());
        if(transaction.isPresent()) {
            Customer customer = transaction.get().getCustomerInfo();
            return CustomerMapper.convertToDto(customer);
        }
        else
            throw new CustomException("There is no transaction for this transactionId : "+request.getTransactionId(), HttpStatus.UNPROCESSABLE_ENTITY);

    }


}
