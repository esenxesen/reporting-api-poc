package com.makeitconsulting.reportingapipoc.model;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "number")
    private String number;

    @Column(name = "expiryMonth")
    private String expiryMonth;

    @Column(name = "expiryYear")
    private String expiryYear;

    @Column(name = "startMonth")
    private String startMonth;

    @Column(name = "startYear")
    private String startYear;

    @Column(name = "issueNumber")
    private String issueNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "gender")
    private String gender;

    @Column(name = "createdAt")
    private Date createdAt;

    @Column(name = "updatedAt")
    private LocalDate updatedAt;

    @Column(name = "deletedAt")
    private Date deletedAt;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_billing_id")
    private Address billingAdressInfo;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_shipping_id")
    private Address shippingAddressInfo;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Address getBillingAdressInfo() {
        return billingAdressInfo;
    }

    public void setBillingAdressInfo(Address billingAdressInfo) {
        this.billingAdressInfo = billingAdressInfo;
    }

    public Address getShippingAddressInfo() {
        return shippingAddressInfo;
    }

    public void setShippingAddressInfo(Address shippingAddressInfo) {
        this.shippingAddressInfo = shippingAddressInfo;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("number", number)
                .add("expiryMonth", expiryMonth)
                .add("startMonth", startMonth)
                .add("startYear", startYear)
                .add("issueNumber", issueNumber)
                .add("email", email)
                .add("birthday", birthday)
                .add("gender", gender)
                .add("createdAt", createdAt)
                .add("updatedAt", updatedAt)
                .add("deletedAt", deletedAt)
                .add("id", id)
                .add("number", number)
                .toString();
    }
}
