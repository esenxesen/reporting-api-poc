package com.makeitconsulting.reportingapipoc.model;

import com.google.common.base.MoreObjects;
import com.makeitconsulting.reportingapipoc.util.ErrorCode;
import com.makeitconsulting.reportingapipoc.util.FilterField;
import com.makeitconsulting.reportingapipoc.util.Keys;
import com.makeitconsulting.reportingapipoc.util.Operation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Currency;
import java.util.Date;
import java.util.Set;

@Entity
public class Transaction {

    @Id
    @Column(name= "transactionId")
    private String transactionId;

    @Column(name= "fromDate")
    private Date fromDate;

    @Column(name= "toDate")
    private Date toDate;

    @Column(name= "status")
    private Keys.StatusKeys status;

    @Enumerated(EnumType.STRING)
    private Operation operation;

    @Column(name= "paymentMethod")
    private Keys.PaymentMethod paymentMethod;

    @Enumerated(EnumType.STRING)
    private ErrorCode errorCode;

    @Enumerated(EnumType.STRING)
    private FilterField filterField;

    @Column(name= "filterValue")
    private String filterValue;

    @ManyToOne
    private Merchant merchant;

    @ManyToOne
    private Acquirer acquirer;

    @ManyToOne
    private Customer customerInfo;

    @ManyToOne
    private Fx fx;

    @ManyToOne
    private MerchantTransactions merchantTransactions;

    @ManyToOne
    private Ipn ipn;


    private Boolean refundable;

    public Boolean getRefundable() {
        return refundable;
    }

    public void setRefundable(Boolean refundable) {
        this.refundable = refundable;
    }

    public Ipn getIpn() {
        return ipn;
    }

    public void setIpn(Ipn ipn) {
        this.ipn = ipn;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Acquirer getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(Acquirer acquirer) {
        this.acquirer = acquirer;
    }

    public Customer getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(Customer customerInfo) {
        this.customerInfo = customerInfo;
    }

    public Fx getFx() {
        return fx;
    }

    public void setFx(Fx fx) {
        this.fx = fx;
    }

    public MerchantTransactions getMerchantTransactions() {
        return merchantTransactions;
    }

    public void setMerchantTransactions(MerchantTransactions merchantTransactions) {
        this.merchantTransactions = merchantTransactions;
    }

    public Keys.StatusKeys getStatus() {
        return status;
    }

    public void setStatus(Keys.StatusKeys status) {
        this.status = status;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Keys.PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Keys.PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public ErrorCode  getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode  errorCode) {
        this.errorCode = errorCode;
    }

    public FilterField getFilterField() {
        return filterField;
    }

    public void setFilterField(FilterField filterField) {
        this.filterField = filterField;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("transactionId", transactionId)
                .add("fromDate", fromDate)
                .add("toDate", toDate)
                .add("status", status)
                .add("operation", operation)
                .add("paymentMethod", paymentMethod)
                .add("errorCode", errorCode)
                .add("filterField", filterField)
                .add("filterValue", filterValue)
                .add("merchant", merchant)
                .add("acquirer", acquirer)
                .add("customerInfo", customerInfo)
                .add("fx", fx)
                .add("merchantTransactions", merchantTransactions)
                .toString();
    }
}
