package com.makeitconsulting.reportingapipoc.model;


import com.google.common.base.MoreObjects;
import com.makeitconsulting.reportingapipoc.util.Keys;
import com.makeitconsulting.reportingapipoc.util.Operation;

import javax.persistence.*;
import java.util.Date;

@Entity
public class MerchantTransactions {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "merchantId")
    private Long merchantId;

    @Column(name = "referenceNo")
    private String referenceNo;
    @Column(name = "status")
    private Keys.StatusKeys status;
    @Column(name = "channel")
    private String channel;
    @Column(name = "customData")
    private String customData;
    @Column(name = "chainId")
    private String chainId;
    @Column(name = "agentInfoId")
    private Long agentInfoId;
    @Column(name = "operation")
    private Operation operation;
    @Column(name = "fxTransactionId")
    private Long fxTransactionId;
    @Column(name = "updatedAt")
    private Date updatedAt;
    @Column(name = "createdAt")
    private Date createdAt;
    @Column(name = "acquirerTransactionId")
    private Long acquirerTransactionId;
    @Column(name = "code")
    private String code;
    @Column(name = "message")
    private String message;
    @Column(name = "transactionId")
    private String transactionId;
    @ManyToOne
    private Agent agent;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Keys.StatusKeys getStatus() {
        return status;
    }

    public void setStatus(Keys.StatusKeys status) {
        this.status = status;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCustomData() {
        return customData;
    }

    public void setCustomData(String customData) {
        this.customData = customData;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public Long getAgentInfoId() {
        return agentInfoId;
    }

    public void setAgentInfoId(Long agentInfoId) {
        this.agentInfoId = agentInfoId;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Long getFxTransactionId() {
        return fxTransactionId;
    }

    public void setFxTransactionId(Long fxTransactionId) {
        this.fxTransactionId = fxTransactionId;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getAcquirerTransactionId() {
        return acquirerTransactionId;
    }

    public void setAcquirerTransactionId(Long acquirerTransactionId) {
        this.acquirerTransactionId = acquirerTransactionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("merchantId", merchantId)
                .add("referenceNo", referenceNo)
                .add("status", status)
                .add("channel", channel)
                .add("customData", customData)
                .add("chainId", chainId)
                .add("agentInfoId", agentInfoId)
                .add("operation", operation)
                .add("fxTransactionId", fxTransactionId)
                .add("updatedAt", updatedAt)
                .add("createdAt", createdAt)
                .add("acquirerTransactionId", acquirerTransactionId)
                .add("code", code)
                .add("message", message)
                .add("transactionId", transactionId)
                .add("agent", agent)
                .toString();
    }

}
