package com.makeitconsulting.reportingapipoc.unitTest.security;

import com.makeitconsulting.reportingapipoc.security.JwtTokenProvider;
import com.makeitconsulting.reportingapipoc.exception.CustomException;
import com.makeitconsulting.reportingapipoc.model.Role;
import com.makeitconsulting.reportingapipoc.security.MyUserDetails;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class JwtTokenProviderTest {

    private final String secretKey = "secret-key";
    private final long TEN_MINUTE = 60000;
    private JwtTokenProvider tokenProvider;


    @Before
    public void setup() {
        tokenProvider = new JwtTokenProvider();
        ReflectionTestUtils.setField(tokenProvider, "secretKey", secretKey);
    }

    @Test
    public void testReturnTrueWhenJWThasValidSignature() {
        boolean isTokenValid = tokenProvider.validateToken(createTokenWithSameSignature());

        assertThat(isTokenValid).isEqualTo(true);
    }

    @Test(expected = CustomException.class)
    public void testReturnExceptionWhenJWTisExpired() {
        ReflectionTestUtils.setField(tokenProvider, "validityInMilliseconds", -TEN_MINUTE);
        List<Role> roleList = new ArrayList<>();
        Role role = Role.ADMIN;
        roleList.add(role);
        String token = tokenProvider.createToken("merchant@test.com", roleList);
        tokenProvider.validateToken(token);

    }

    @Test(expected = CustomException.class)
    public void testReturnExceptionWhenJWTisInvalid() {
        boolean isTokenValid = tokenProvider.validateToken("");

        assertThat(isTokenValid).isEqualTo(false);
    }


    private String createTokenWithSameSignature() {
        Date now = new Date();
        Date validity = new Date(now.getTime() + TEN_MINUTE);

        return Jwts.builder()
                .setSubject("test")
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

}
