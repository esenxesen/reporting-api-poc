package com.makeitconsulting.reportingapipoc.unitTest.security;

import com.makeitconsulting.reportingapipoc.repository.UserRepository;
import com.makeitconsulting.reportingapipoc.security.JwtTokenFilter;
import com.makeitconsulting.reportingapipoc.security.JwtTokenFilterConfigurer;
import com.makeitconsulting.reportingapipoc.security.JwtTokenProvider;
import com.makeitconsulting.reportingapipoc.security.MyUserDetails;
import com.makeitconsulting.reportingapipoc.model.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class JwtTokenFilterTest {

    @InjectMocks
    private JwtTokenProvider tokenProvider;

    private JwtTokenFilter jwtFilter;


    @Mock
    private MyUserDetails myUserDetails;

    @Mock
    private UserRepository userRepository;
    @Before
    public void setup() {
        tokenProvider = new JwtTokenProvider();
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(tokenProvider, "secretKey", "test secret");
        ReflectionTestUtils.setField(tokenProvider, "validityInMilliseconds", 60000);

        jwtFilter = new JwtTokenFilter(tokenProvider);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    public void testJWTFilter() throws Exception {
        List<Role> roleList = new ArrayList<>();
        Role role = Role.ADMIN;
        roleList.add(role);

     Mockito.when(myUserDetails.loadUserByUsername("merchant@test.com")).thenReturn(
                User.withUsername("merchant@test.com")//
                        .password("12345")
                        .authorities(roleList)//
                        .accountExpired(false)//
                        .accountLocked(false)//
                        .credentialsExpired(false)//
                        .disabled(false)//
                        .build());


        roleList.add(role);
        String token = tokenProvider.createToken("merchant@test.com", roleList);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(JwtTokenFilterConfigurer.AUTHORIZATION_HEADER, "Bearer " + token);
        request.setRequestURI("/api/test");
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain filterChain = new MockFilterChain();
        jwtFilter.doFilter(request, response, filterChain);
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication().getName()).isEqualTo("merchant@test.com");
    }
}
