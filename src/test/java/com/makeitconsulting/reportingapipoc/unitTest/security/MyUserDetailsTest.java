package com.makeitconsulting.reportingapipoc.unitTest.security;


import com.makeitconsulting.reportingapipoc.model.Address;
import com.makeitconsulting.reportingapipoc.model.Customer;
import com.makeitconsulting.reportingapipoc.model.Role;
import com.makeitconsulting.reportingapipoc.model.User;
import com.makeitconsulting.reportingapipoc.repository.AddressRepository;
import com.makeitconsulting.reportingapipoc.repository.UserRepository;
import com.makeitconsulting.reportingapipoc.security.MyUserDetails;
import com.makeitconsulting.reportingapipoc.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MyUserDetailsTest {

    @InjectMocks
    private MyUserDetails userDetails;


    @Mock
    private UserService userService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;

    private User admin;
    @Before
    public void init() {


        MockitoAnnotations.initMocks(this);


    }

    @Test
    @Transactional
    public void assertThatUserCanBeFoundByLogin() {

        admin = new User();
        admin.setEmail("merchant@test.com");
        admin.setRoles(new ArrayList<Role>(Arrays.asList(Role.ADMIN)));
        admin.setPassword("12345");
        Mockito.when(userRepository.findByEmail("merchant@test.com")).thenReturn(admin);

        UserDetails user =  userDetails.loadUserByUsername("merchant@test.com");
        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo("merchant@test.com");
    }


}
