package com.makeitconsulting.reportingapipoc.unitTest.controller;

import com.makeitconsulting.reportingapipoc.ReportingApiPocApplication;
import com.makeitconsulting.reportingapipoc.controller.CustomerController;
import com.makeitconsulting.reportingapipoc.dto.request.TransactionIdRequest;
import com.makeitconsulting.reportingapipoc.dto.response.CustomerResponse;
import com.makeitconsulting.reportingapipoc.exception.CustomException;
import com.makeitconsulting.reportingapipoc.service.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReportingApiPocApplication.class)
public class CustomerControllerTest {

    MockMvc mockMvc;
    @Autowired
    CustomerService customerService;


    private final String jsonBody = "{\"transactionId\": \"1-1444392550-0\"}";
    TransactionIdRequest transactionIdRequest = new TransactionIdRequest();

    @Before
    public void preTest(){
        MockitoAnnotations.initMocks(this);
        CustomerController customerController = new CustomerController(customerService);
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();

    }

    @Test
    public void testGetCustomerInfo() throws Exception {

        transactionIdRequest.setTransactionId("1-1444392550-0");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parsedDate = formatter.parse("1986-03-20 12:09:10");

        CustomerResponse customer = new CustomerResponse();
        customer.setNumber("401288XXXXXX1881");
        customer.setExpiryMonth("6");
        customer.setExpiryYear("2017");
        customer.setEmail("micheal@gmail.com");
        customer.setCreatedAt(new Date());
        customer.setUpdatedAt(LocalDate.now());
        customer.setBirthday(parsedDate);

        customer.setBillingFirstName("Micheal");
        customer.setBillingLastName("Kara");
        customer.setBillingAddress1("test address");
        customer.setBillingCity("Antalya");
        customer.setBillingPostcode("07070");
        customer.setBillingCountry("TR");

        customer.setShippingFirstName("Micheal");
        customer.setShippingLastName​("Kara");
        customer.setShippingAddress1​("test address");
        customer.setShippingCity​("Antalya");
        customer.setShippingPostcode​("07070");
        customer.setShippingCountry("TR");


        mockMvc.perform(post("/api/v3/client")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andDo(print())
                .andExpect(status().isOk());

        CustomerResponse response = customerService.getCustomerInfo(transactionIdRequest);

        assertThat(response.getNumber()).isEqualTo(customer.getNumber());
        assertThat(response.getExpiryMonth()).isEqualTo(customer.getExpiryMonth());

    }

    @Test(expected = CustomException.class)
    public void testGetCustomerInfoExceotion(){
        transactionIdRequest.setTransactionId("transactionId");
        customerService.getCustomerInfo(transactionIdRequest);

    }

}
