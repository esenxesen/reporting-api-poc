package com.makeitconsulting.reportingapipoc.unitTest.controller;

import com.makeitconsulting.reportingapipoc.ReportingApiPocApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReportingApiPocApplication.class)
public class TransactionControllerTest {

    @Test
    public void test(){
        assertThat(1).isEqualTo(1);
    }

}
