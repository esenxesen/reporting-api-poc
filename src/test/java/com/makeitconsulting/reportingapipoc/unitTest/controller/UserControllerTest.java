package com.makeitconsulting.reportingapipoc.unitTest.controller;


import com.makeitconsulting.reportingapipoc.ReportingApiPocApplication;
import com.makeitconsulting.reportingapipoc.controller.UserController;
import com.makeitconsulting.reportingapipoc.dto.request.UserLoginRequest;
import com.makeitconsulting.reportingapipoc.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.isEmptyString;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReportingApiPocApplication.class)
public class UserControllerTest {

    MockMvc mockMvc;

    @Mock
    UserService userService;

    @InjectMocks
    UserController userController;

    private final String jsonBody = "{\"email\": \"esen\", \"password\": \"pass\"}";
    UserLoginRequest request = new UserLoginRequest();

    @Before
    public void preTest(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
        request.setEmail("esen");
        request.setPassword("pass");
    }
    @Test
    public void testLogin() throws Exception {
        Mockito.when(userService.login(request.getEmail(),request.getPassword())).thenReturn("jwtToken");
        mockMvc.perform(post("/api/v3/merchant/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andDo(print())
                .andExpect(content().json("{\"token\": \"Bearer jwtToken\", \"status\": \"APPROVED\"}"))
                .andExpect(jsonPath("$.token").isString())
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andExpect(jsonPath("$.status").isNotEmpty())
                .andExpect(jsonPath("$.status").isString())
                .andExpect(header().string("Authorization", not(nullValue())))
                .andExpect(header().string("Authorization", not(isEmptyString())));

    }



}
